use actix::dev::{MessageResponse, ResponseChannel};
use actix::{Actor, Context, Handler, Message};

#[derive(Message)]
#[rtype(result = "Res")]
enum Msg {
    Ping,
    Pong,
}

enum Res {
    GotPing,
    GotPong,
}

impl<A, M> MessageResponse<A, M> for Res
where
    A: Actor,
    M: Message<Result = Res>,
{
    fn handle<R: ResponseChannel<M>>(
        self,
        _ctx: &mut A::Context,
        tx: Option<R>,
    ) {
        if let Some(tx) = tx {
            tx.send(self);
        }
    }
}

struct PingActor;

impl Actor for PingActor {
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        println!("Actor started.");
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        println!("Actor stopped.");
    }
}

impl Handler<Msg> for PingActor {
    type Result = Res;

    fn handle(&mut self, msg: Msg, _ctx: &mut Self::Context) -> Self::Result {
        match msg {
            Msg::Ping => Res::GotPing,
            Msg::Pong => Res::GotPong,
        }
    }
}

#[actix_rt::main]
async fn main() {
    // start new actor
    let addr = PingActor.start();

    // Send Ping messages
    let ping_feuture = addr.send(Msg::Ping).await;
    let pong_feuture = addr.send(Msg::Pong).await;

    match pong_feuture {
        Ok(res) => match res {
            Res::GotPing => println!("Ping received!"),
            Res::GotPong => println!("Pong received!"),
        },
        Err(e) => println!("Actor is probably dead: {}", e),
    }

    match ping_feuture {
        Ok(res) => match res {
            Res::GotPing => println!("Ping received!"),
            Res::GotPong => println!("Pong received!"),
        },
        Err(e) => println!("Actor is probably dead: {}", e),
    }
}
