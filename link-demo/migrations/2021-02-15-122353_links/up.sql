-- Your SQL goes here

CREATE TABLE `links` (`id` integer NOT NULL PRIMARY KEY
                      , `link` text NOT NULL
                      , `title` text NOT NULL
                      , `date_created` text NOT NULL);