-- Add migration script here

CREATE TABLE users (id blob PRIMARY KEY NOT NULL
                    , username varchar NOT NULL
                    , email varchar NOT NULL
                    , password_hash varchar NOT NULL
                    , full_name varchar NULL
                    , bio varchar NULL
                    , image varchar NULL
                    , email_verified boolean NOT NULL DEFAULT FALSE
                    , active boolean NOT NULL DEFAULT TRUE
                    , created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                    , updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP);


CREATE UNIQUE INDEX idx_users_username ON users (username);
