pub mod crypto;

use std::sync::Arc;

use color_eyre::Result;
use dotenv::dotenv;
use eyre::WrapErr;
use serde::Deserialize;
use sqlx::SqlitePool;
use tracing::{info, instrument};
use tracing_subscriber::EnvFilter;

use self::crypto::CryptoService;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub host: String,
    pub port: u32,
    pub database_url: String,
    pub secret_key: String,
}

impl Config {
    #[instrument]
    pub fn from_env() -> Result<Config> {
        dotenv().ok();

        tracing_subscriber::fmt()
            .with_env_filter(EnvFilter::from_default_env())
            .init();

        info!("Loading configuration");

        let mut c = config::Config::new();

        c.merge(config::Environment::default())?;

        c.try_into()
            .context("loading configuration from environment")
    }

    pub async fn db_pool(&self) -> Result<SqlitePool> {
        info!("Creating database connection pool.");

        SqlitePool::connect(&self.database_url)
            .await
            .context("creating database connection pool")
    }

    pub fn crypto_service(&self) -> CryptoService {
        CryptoService {
            key: Arc::new(self.secret_key.clone()),
        }
    }
}
