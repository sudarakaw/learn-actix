use actix_web::{
    web::{self, ServiceConfig},
    HttpResponse, Responder,
};

pub fn app_config(config: &mut ServiceConfig) {
    let health_resource = web::resource("/").route(web::get().to(health));

    config.service(health_resource);
}

pub async fn health() -> impl Responder {
    HttpResponse::Ok().finish()
}
