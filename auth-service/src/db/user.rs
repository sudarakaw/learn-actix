use std::sync::Arc;

use color_eyre::Result;
use sqlx::SqlitePool;

use crate::{
    config::crypto::CryptoService,
    models::user::{NewUser, User},
};

pub struct UserRepository {
    pool: Arc<SqlitePool>,
}

impl UserRepository {
    pub fn new(pool: Arc<SqlitePool>) -> Self {
        Self { pool }
    }

    pub async fn create(&self, new_user: NewUser, crypto_service: &CryptoService) -> Result<User> {
        let password_hash = crypto_service.hash_password(new_user.password).await?;

        let user = sqlx::query_as::<_, User>(
            "insert into users (username, email, password_hash) values($1, $2, $3)",
        )
        .bind(new_user.username)
        .bind(new_user.email)
        .bind(password_hash)
        .fetch_one(&*self.pool)
        .await?;

        Ok(user)
    }
}
