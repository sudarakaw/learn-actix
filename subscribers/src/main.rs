use actix::{Actor, Context, Handler, Message, Recipient, System};

#[derive(Message)]
#[rtype(result = "()")]
struct OrderShipped(usize);

#[derive(Message)]
#[rtype(result = "()")]
struct Ship(usize);

/// Subsctibe to order ship message
#[derive(Message)]
#[rtype(result = "()")]
struct Subscribe(Recipient<OrderShipped>);

/// Actor that provides order shipped event subscription
struct OrderEvents {
    subscribers: Vec<Recipient<OrderShipped>>,
}

impl OrderEvents {
    fn new() -> Self {
        OrderEvents {
            subscribers: vec![],
        }
    }

    /// Send event to all subscribers
    fn notify(&mut self, order_id: usize) {
        for subscriber in &self.subscribers {
            let _ = subscriber.do_send(OrderShipped(order_id));
        }
    }
}

impl Actor for OrderEvents {
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        println!("OrderEvents started.");
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        println!("OrderEvents stopped.");
    }
}

/// Subscribe to shipment event
impl Handler<Subscribe> for OrderEvents {
    type Result = ();

    fn handle(
        &mut self,
        msg: Subscribe,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        self.subscribers.push(msg.0);
    }
}

impl Handler<Ship> for OrderEvents {
    type Result = ();

    fn handle(&mut self, msg: Ship, _ctx: &mut Self::Context) -> Self::Result {
        println!("Sending notifications for order {}", msg.0);
        self.notify(msg.0);

        System::current().stop();
    }
}

/// Email subscriber
struct EmailSubscriber;

impl Actor for EmailSubscriber {
    type Context = Context<Self>;
}

impl Handler<OrderShipped> for EmailSubscriber {
    type Result = ();

    fn handle(
        &mut self,
        msg: OrderShipped,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        println!("Email sent for order {}", msg.0);
    }
}

/// Sms subscriber
struct SmsSubscriber;

impl Actor for SmsSubscriber {
    type Context = Context<Self>;
}

impl Handler<OrderShipped> for SmsSubscriber {
    type Result = ();

    fn handle(
        &mut self,
        msg: OrderShipped,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        println!("Sms sent for order {}", msg.0);
    }
}

fn main() {
    let sys = System::new("events");

    let email_subscriber = Subscribe(EmailSubscriber {}.start().recipient());
    let sms_subscriber = Subscribe(SmsSubscriber {}.start().recipient());
    let order_event = OrderEvents::new().start();

    order_event.do_send(email_subscriber);
    order_event.do_send(sms_subscriber);
    order_event.do_send(Ship(1));
    order_event.do_send(Ship(2));
    order_event.do_send(Ship(3));

    let _ = sys.run();
}
