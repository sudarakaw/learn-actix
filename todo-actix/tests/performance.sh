#!/usr/bin/env sh

ab \
  -p ./tests/todo.json \
  -T "application/json" \
  -n 100000 \
  -k -q \
  -c 30 \
  http://127.0.0.1:5000/todos
