#!/usr/bin/env sh

docker run \
  --rm \
  -it \
  -p 5432:5432 \
  -e POSTGRES_PASSWORD=actix \
  -e POSTGRES_USER=suda \
  -e POSTGRES_DB=todo \
  postgres:11-alpine
