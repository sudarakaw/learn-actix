use actix_web::{web, HttpResponse, Responder};
use deadpool_postgres::{Client, Pool};
use slog::{crit, error, o, Logger};

use crate::db;
use crate::errors::AppError;
use crate::models::{AppState, ResultResponse, Status, TodoListNew};

async fn get_client(pool: Pool, log: Logger) -> Result<Client, AppError> {
    pool.get().await.map_err(|err| {
        let sublog = log.new(o!("cause" => err.to_string()));

        crit!(sublog, "Error creating client");

        AppError::db_error(err)
    })
}

fn log_error(log: Logger) -> Box<dyn Fn(AppError) -> AppError> {
    Box::new(move |err| {
        let sublog = log.new(o!("cause" => err.cause.clone()));

        error!(sublog, "{}", err.message());

        err
    })
}

pub async fn status() -> impl Responder {
    HttpResponse::Ok().json(Status {
        status: "UP".to_string(),
    })
}

pub async fn get_todos(
    state: web::Data<AppState>,
) -> Result<impl Responder, AppError> {
    let log = state.log.new(o!("handler" => "get_todos"));

    let client: Client = get_client(state.pool.clone(), log.clone()).await?;

    db::get_todos(&client)
        .await
        .map(|todos| HttpResponse::Ok().json(todos))
        .map_err(log_error(log))
}

pub async fn create_todo(
    state: web::Data<AppState>,
    json: web::Json<TodoListNew>,
) -> Result<impl Responder, AppError> {
    let log = state.log.new(o!("handler" => "get_todos"));

    let client: Client = get_client(state.pool.clone(), log.clone()).await?;

    db::create_todo(&client, json.title.clone())
        .await
        .map(|todo| HttpResponse::Created().json(todo))
        .map_err(log_error(log))
}

pub async fn get_items(
    state: web::Data<AppState>,
    web::Path((list_id, _)): web::Path<(i32, ())>,
) -> Result<impl Responder, AppError> {
    let log = state.log.new(o!("handler" => "get_todos"));

    let client: Client = get_client(state.pool.clone(), log.clone()).await?;

    db::get_items(&client, list_id)
        .await
        .map(|items| HttpResponse::Ok().json(items))
        .map_err(log_error(log))
}

pub async fn check_item(
    state: web::Data<AppState>,
    web::Path((list_id, item_id)): web::Path<(i32, i32)>,
) -> Result<impl Responder, AppError> {
    let log = state.log.new(o!("handler" => "get_todos"));

    let client: Client = get_client(state.pool.clone(), log.clone()).await?;

    db::check_item(&client, list_id, item_id)
        .await
        .map(|updated| {
            HttpResponse::Ok().json(ResultResponse { success: updated })
        })
        .map_err(log_error(log))
}
