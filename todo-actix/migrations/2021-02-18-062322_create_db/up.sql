-- Your SQL goes here

CREATE TABLE todo_list (id serial PRIMARY KEY
                        , title varchar(128) NOT NULL);


CREATE TABLE todo_item (id serial PRIMARY KEY
                        , title varchar(128) NOT NULL
                        , CHECKED boolean NOT NULL DEFAULT FALSE
                        , list_id integer NOT NULL
                        ,
                        FOREIGN KEY (list_id) REFERENCES todo_list(id));