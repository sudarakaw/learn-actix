use actix::{Actor, ActorContext, Context, Handler, Message, Running, System};

#[derive(Message)]
#[rtype(result = "usize")]
struct PingMsg(usize);

struct PingActor {
    count: usize,
}

impl Actor for PingActor {
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        println!("Actor started.");
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        println!("Actor stopping at count {}", self.count);

        if self.count > 25 {
            Running::Stop
            // Running::Continue
        } else {
            Running::Continue
        }
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        println!("Actor stopped.");
    }
}

impl Handler<PingMsg> for PingActor {
    type Result = usize;

    fn handle(
        &mut self,
        msg: PingMsg,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        self.count += msg.0;

        ctx.stop();

        self.count
    }
}

#[actix_rt::main]
async fn main() {
    // start new actor
    let addr = PingActor { count: 10 }.start();

    // Send some messages
    let _ = addr.send(PingMsg(10)).await;
    // Synchronous
    let _ = addr.do_send(PingMsg(8));
    let _ = addr.send(PingMsg(3)).await;
    let res = addr.send(PingMsg(5)).await;

    println!("Result: {:?}", res);

    System::current().stop();
}
