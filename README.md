# Practice Code: Actix (web) - Rust Actor Framework

Practice code created while learning [Actix](https://actix.rs/), a actor model
based [Rust](https://www.rust-lang.org/) web server framework.

## Projects

**1. link-demo** - Example project from [Actix-web with Diesel and Sqlite](https://www.youtube.com/playlist?list=PLBok0UdvO6519tl0LzER8w_J-0pcro7q9)
by [kilroyjones](https://github.com/kilroyjones).

**2. todo-actix** - Example project from [Creating a TODO service with Actix Web](https://www.youtube.com/playlist?list=PLECOtlti4PspZHOySyzf9RApJFguZPFoF)
by [nemesiscodex](https://www.patreon.com/nemesiscodex).

**3. ws-demo** - Example project from [Rust - Websockets in Actix Web - Full Tutorial](https://www.youtube.com/watch?v=XjkB_O_rRgc)
by [Anthony Oleinik](https://anth-oleinik.medium.com/about).

**4. ping (v1, v2)** - Example project from [Actix book](https://actix.rs/book/actix/)
by [Nikolay Kim](https://github.com/fafhrd91) and [community](https://github.com/actix/book).

**5. subscribers** - Example project from [Actix book](https://actix.rs/book/actix/)
by [Nikolay Kim](https://github.com/fafhrd91) and [community](https://github.com/actix/book).

**6. auth-service** - Example project from [Building an Authentication Service using Actix](https://www.youtube.com/playlist?list=PLECOtlti4Psqw1qRaN4R9sWSQWvqfJU_V)
by [nemesiscodex](https://www.patreon.com/nemesiscodex).
_Note: this project is incomplete._
